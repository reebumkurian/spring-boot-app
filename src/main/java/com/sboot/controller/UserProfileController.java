package com.sboot.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserProfileController {


    @RequestMapping("/test")
    public String test(@RequestParam("name") String name) {
        return "Name : " + name ;
    }
}
